FROM node:10.14-alpine
RUN mkdir /code
WORKDIR /code
COPY . /code/
RUN npm install
