import Vue from 'vue'
import Router from 'vue-router'

import AboutPage from './pages/AboutPage'
import HomePage from './pages/HomePage'


Vue.use(Router)

export default new Router({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'home',
            component: HomePage,
        },
        {
            path: '/about',
            name: 'about',
            component: AboutPage,
        },
    ]
})
