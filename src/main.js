import Vue from 'vue'
import App from './App.vue'
import router from './router'

Vue.config.productionTip = false

Vue.prototype.$hostname = (Vue.config.productionTip) ? 'http://api.cherrychart.io/apple' : 'http://localhost:8000/apple'

Vue.prototype.$hostname = 'http://api.cherrychart.io/apple'

new Vue({
  router,
  render: h => h(App),
}).$mount('#app')
